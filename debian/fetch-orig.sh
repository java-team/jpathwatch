#!/bin/sh

set -e

NAME=jpathwatch
VERSION=0.95
DEB_VERSION=${VERSION}

OUT_DIR=${NAME}-${VERSION}

downloadSnapshot() {
	VERSION_DASH=$(echo $VERSION | sed "s/\./-/g")
	SRC_DIR=jpathwatch

	rm -rf $OUT_DIR
	svn export https://jpathwatch.svn.sourceforge.net/svnroot/jpathwatch/tags/$VERSION_DASH/$SRC_DIR
	mv $SRC_DIR $OUT_DIR
}

downloadSnapshot

# Remove empty directories
find $OUT_DIR -type d -empty -delete

ORIG_TARBALL=${NAME}_${DEB_VERSION}.orig.tar.xz

echo "Creating tarball '$ORIG_TARBALL'..."
tar -cJf ../$ORIG_TARBALL $OUT_DIR

rm -rf $OUT_DIR
